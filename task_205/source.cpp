//lca двоичным подъемом, обход в глубину
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<int, int> pii;

int n, a, b, timer;
vector<vector<int> > G;
const int N = 1e5 + 228;
const int LG = 18;
vector<int> dep, in, out;

int up[LG][N];

void dfs(int a, int p) {
	in[a] = timer++;
	up[0][a] = p;
	for (int i = 1; i < LG; i++) {
		up[i][a] = up[i - 1][up[i - 1][a]];
	}
	for (int to : G[a]) {
		if (to != p) {
			dep[to] = dep[a] + 1;
			dfs(to, a);
		}
	}
	out[a] = timer;
}

bool upper(int u, int v) {
	return in[u] <= in[v] && out[v] <= out[u];
}

int lca(int u, int v) {
	if (in[u] > in[v]) {
		swap(u, v);
	}
	if (upper(u, v)) {
		return u;
	}
	for (int i = LG - 1; i >= 0; i--) {
		if (!upper(up[i][u], v)) {
			u = up[i][u];
		}
	}
	return up[0][u];
}

int dist(int u, int v) {
	int t = lca(u, v);
	return dep[u] + dep[v] - 2 * dep[t];
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    cin >> n;
    G.resize(n);
    for0(i, n-1){
        cin >> a >> b;
        a--;
        b--;
        G[a].pb(b);
        G[b].pb(a);
    }
    dep.resize(n, 0);
	in.resize(n);
	out.resize(n);
	dfs(0, 0);

	int ans = 0;
	for0(i, n){
        ans += dist(i, (i+1)%n);
	}

	cout<<ans;
}