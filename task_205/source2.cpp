// lca двоичным подъемом, обход в ширину без глубокой рекурсии
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<int, int> pii;

int n, a, b, timer;
vector<vector<int> > G;
const int N = 1e5 + 228;
const int LG = 18;
vector<int> dep, in, out;

int up[LG][N];

bool upper(int u, int v) {
	return in[u] <= in[v] && out[v] <= out[u];
}

int lca(int u, int v) {
	if (in[u] > in[v]) {
		swap(u, v);
	}
	if (upper(u, v)) {
		return u;
	}
	for (int i = LG - 1; i >= 0; i--) {
		if (!upper(up[i][u], v)) {
			u = up[i][u];
		}
	}

	return up[0][u];
}

int dist(int u, int v) {
	int t = lca(u, v);
	return dep[u] + dep[v] - 2 * dep[t];
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    cin >> n;
    G.resize(n);
    for0(i, n-1){
        cin >> a >> b;
        a--;
        b--;
        G[a].pb(b);
        G[b].pb(a);
    }
    dep.resize(n, 0);
	in.resize(n);
	out.resize(n, 0);

	deque<int> och;
	vector<int> par(n), d(n, 1), oo;
	och.pb(0);
	oo.pb(0);
	dep[0] = 0;
	par[0] = 0;
	while(sz(och) > 0){
      int tb = och[0];
      och.pop_front();
      for(auto to : G[tb])  if(to != par[tb]){
        dep[to] = dep[tb] + 1;
        par[to] = tb;

        	up[0][to] = tb;
	for (int i = 1; i < LG; i++) {
		up[i][to] = up[i - 1][up[i - 1][to]];
	}
        och.pb(to);
        oo.pb(to);
      }

	}
	for(int i = sz(oo)-1; i >= 1; i--)
        d[par[oo[i]]] += d[oo[i]];

    in[0] = 0;
    for(int i = 1; i < sz(oo); i++){
        in[oo[i]] = out[par[oo[i]]]+1;
        out[oo[i]] = in[oo[i]] + 1;
        out[par[oo[i]]] += 2*d[oo[i]];
    }

	int ans = 0;
	for0(i, n){
        ans += dist(i, (i+1)%n);
	}
	cout<<ans;
}