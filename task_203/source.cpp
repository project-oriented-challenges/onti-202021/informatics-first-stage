//граф на буквах, обход в глубину
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<int, int> pii;

map<char, set<char> > G;
map<char, int> rzb;
int num = 0;
vector<set<char> > toans;
set<char> emp;

void dfs(char a){
    toans[rzb[a]].insert(a);
    for(auto to : G[a]) if(rzb[to] == 0) {
        rzb[to] = rzb[a];
        dfs(to);
    }
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    int p;
    cin >> p;
    string s;
    cin >> s;

    vector<set<char> > op(p);

    for0(i, sz(s))  op[i%p].insert(s[i]);

    for0(i, p)
        for(auto q : op[i])
        for(auto w : op[i]) {
            G[q].insert(w);
            G[w].insert(q);
        }

        toans.pb(emp);
        for(auto q : G)
        if(rzb[q.x] == 0){
            num++;
            toans.pb(emp);
            rzb[q.x] = num;
            dfs(q.x);
        }

    for0(i, sz(s)) s[i] = *(toans[rzb[s[i]]].begin());
    cout<<s;
}