from bisect import bisect_left

def binary_search(a, x):
    i = bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return i
    else:
        return -1

n = int(input())
dividers = []
for i in range(1, math.ceil(math.sqrt(n))):
    if n % i == 0:
        dividers.append(i)
divs = dividers.copy()
if int(math.sqrt(n)) ** 2 == n:
    dividers.append(int(math.sqrt(n)))
for x in divs[::-1]:
    dividers.append(n // x)
dp = [[0 for i in range(len(dividers))] for j in range(len(dividers))]
dp[0][0] = 1
for a in range(len(dividers) - 1):
    pref_sum = 0
    for i in range(len(dividers) - 1):
        pref_sum += dp[a][i]
        dp[a][i] = pref_sum
        if dividers[a + 1] % dividers[i + 1] == 0:
            bs = binary_search(dividers, dividers[a + 1] // dividers[i + 1])
            if bs != -1:
                dp[a + 1][i + 1] = dp[bs][i]
print(sum(dp[-1]))