//Динамическое программирование
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;

vector<pii> raz;
set<int> del;
vector<int> rdel;

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    int n, dn;
    cin >> n;
    dn = n;

    for(int i = 2; i <= 1e6+1 && dn > 1; i++) if(dn % i == 0){
        int t = 0;
            while(dn % i == 0){
                t++;
                dn /= i;
            }
        raz.pb({i, t});
    }

    if(dn > 1) raz.pb({dn, 1});

    del.insert(1);

    for(auto q : raz){
        for0(i, q.y){
            rdel.resize(0);
            for(auto w : del) rdel.pb(w * q.x);
            for(auto w : rdel) del.insert(w);
        }
    }

    rdel.resize(0);
    for(auto q : del) rdel.pb(q);
    int r = sz(rdel);

    map<int, int> rev;
    for0(i, r) rev[rdel[i]] = i;

    vector<vector<int> > dp(r, vector<int>(r, 0));

    dp[0][0] = 1;
    for(int i = 1; i < r; i++) dp[i][0] += dp[i-1][0];

    for(int j = 1; j < r; j++){
        for(int i = 1; i < r; i++) if(rdel[j] % rdel[i] == 0)
            dp[i][j] = dp[i-1][rev[rdel[j] / rdel[i]]];
        for(int i = 1; i < r; i++) dp[i][j] += dp[i-1][j];

    }
    cout<<dp[r-1][r-1];
}
