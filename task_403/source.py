def get_unwatched(verts):
    i = 0
    for vert in verts:
        if not vert['watched']:
            vert['orient'] = 1
            return vert

def task1():
    vert_num = int(input())
    edges = int(input())
    verts = [{"edges": [], "orient": 0, "watched": False} for _ in range(vert_num)]
    for _ in range(edges):
        a, b = (int(i)-1 for i in input().split())
        verts[a]["edges"].append(b)
        verts[b]["edges"].append(a)

    stack = []
    while vert_num > 0:
        if len(stack) == 0:
            stack.append(get_unwatched(verts))
        vert = stack.pop()
        vert_num -= 1
        vert["watched"] = True
        for next_v in vert['edges']:
            if not verts[next_v]["watched"] and verts[next_v] not in stack:
                stack.append(verts[next_v])
            if verts[next_v]["orient"] != 0 and verts[next_v]["orient"] + vert['orient'] != 3:
                return 'bad'
            elif verts[next_v]["orient"] == 0:
                verts[next_v]["orient"] = 3 - vert['orient']
    return 'good'

print(task1())