def get_borders(l, sym=1):
    a, b = None, None
    for i in range(len(l)):
        if sym in l[i]:
            a = i
            break
    for i in range(len(l)-1, -1, -1):
        if sym in l[i]:
            b = i
            break
    return a, b

def get_lr(l, sym=1):
    left, right = None, None
    for i in range(len(l)):
        if l[i] == sym:
            left = i
            break
    for i in range(len(l)-1, -1, -1):
        if l[i] == sym:
            right = i
            break
    return left, right

def get_rectangle(l, b):
    result = []
    for i in range(b[0], b[2]+1):
        tmp = []
        for j in range(b[1], b[3]+1):
            tmp.append(l[i][j])
        result.append(tmp)
    return result

def is_rectangle(l, sym=1):
    up, bottom = get_borders(l, sym)
    if up is None:
        return None
    left, right = get_lr(l[up], sym)
    for i in range(up, bottom+1):
        if (left, right) != get_lr(l[i], sym):
            return None
    return (up, left, bottom, right)

def task5():
    rows = []
    for _ in range(10):
        rows.append([int(i) for i in input()])
    a = is_rectangle(rows)
    if a:
        tmp = get_rectangle(rows, a)
        if is_rectangle(tmp, 0):
            return '0'
        for i in range(len(tmp)):
            if is_rectangle(tmp[:i], 0) and is_rectangle(tmp[i:], 0):
                return '8'
        if a[2]-a[0] > a[3]-a[1]:
            return '1'
        if a[2]-a[0] < a[3]-a[1]:
            return '-'
    else:
        up, bottom = get_borders(rows)
        for i in range(up, bottom+1):
            a = is_rectangle(rows[:i])
            b = is_rectangle(rows[i:])
            if a and b and a[1] < b[1] and a[3] > b[3]:
                return 'T'
    return 'X'

print(task5())