def balancedBrackets(expr):
    stack = []
    for char in expr:
        if char in ['(','{','[']:
            stack.append(char)
        elif char in [')','}', ']']:
            if(len(stack) == 0):
                return False
            top = stack.pop()
            if(top == '(' and char != ')'):
                return ""
            if(top == '{' and char != '}' ):
                return False
            if(top == '[' and char != ']'):
                return False
        else:
            continue
    if(len(stack) > 0):
        return False
    return True

expr = input()
print("correct") if balancedBrackets(expr) else print("incorrect")