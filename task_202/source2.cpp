//вещественно, бинпоиском точку пересечения
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<ld, ld> pld;

ld eps = 1e-7;

struct line{
    ld A, B, C;
};

ld dist(pld a, pld b){
    return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}

line L(pld a1, pld a2){
    ld A = a2.y - a1.y;
    ld B = a1.x - a2.x;
    ld C = a2.x*a1.y - a1.x*a2.y;

    line r = {A, B, C};
    return r;
}

int sgn(ld x){
    if(x < -eps) return -1;
    if(x > eps) return 1;
    return 0;
}

pld tp(pld A, pld B, pld s, pld p){
    line AB = L(A, B);

    ld L = 0, R = 1e6;

    pld v = {p.x - s.x, p.y - s.y};
    pld ab = {B.x - A.x, B.y - A.y};
    ld Fs = AB.A * (s.x + L * ab.x) + AB.B * (s.y + L * ab.y) + AB.C;

    if(abs(v.x*ab.y - v.y*ab.x) < eps) return{1e6, 1e6};

    while(R - L > eps){
        ld M = (L+R)/2.0;

        pld m = {s.x + M*v.x, s.y + M*v.y};

        ld F = AB.A * m.x + AB.B * m.y + AB.C;

        if(sgn(F) == sgn(Fs)) L = M; else R = M;
    }

    return {s.x + L*v.x, s.y + L*v.y};
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    pld A, B;
    cin >> A.x >> A.y >> B.x >> B.y;

    int n, m;
    cin >> n >> m;
    vector<pld> s(n), p(m);
    for0(i, n) cin >> s[i].x >> s[i].y;
    for0(i, m) cin >> p[i].x >> p[i].y;

    int ans = 0;

    for0(i, n)
    for0(j, m){
        line AB = L(A, B);
        ld tos = AB.A * s[i].x + AB.B * s[i].y + AB.C;
        ld top = AB.A * p[j].x + AB.B * p[j].y + AB.C;

        if(sgn(tos) == sgn(top)){

            pld w = tp(A, B, s[i], p[j]);

            if(abs(w.x) < 1e6 && abs(w.y) < 1e6){

                if(abs(dist(A, w) + dist(w, B) - dist(A, B)) < eps) {
                        ans++;
                }
            }

        }
    }

    cout << ans;
}