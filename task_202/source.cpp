//Целочисленно, сравнение площадей
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<int, int> pii;

int sqt2(pii a, pii b, pii c){
    pii v1 = {b.x - a.x, b.y - a.y};
    pii v2 = {c.x - a.x, c.y - a.y};

    return abs(v1.x*v2.y - v1.y*v2.x);
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    pii A, B;
    cin >> A.x >> A.y >> B.x >> B.y;

    int n, m;
    cin >> n >> m;
    vector<pii> s(n), p(m);
    for0(i, n) cin >> s[i].x >> s[i].y;
    for0(i, m) cin >> p[i].x >> p[i].y;

    int ans = 0;

    for0(i, n){
        int S = sqt2(A, B, s[i]);
        for0(j, m){
           int s1 = sqt2(A, B, p[j]);
           int s2 = sqt2(A, p[j], s[i]);
           int s3 = sqt2(B, p[j], s[i]);
           if(s1 + s2 + s3 == S) ans++;
        }
    }
    cout << ans;
}