//Метод карманов
#include<bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    int u, p;
    cin >> u >> p;
        while(u--){
            string s;
            cin >> s;
            vector<set<char> > v(p);
            for0(i, sz(s))
              if(s[i] != '?') v[i%p].insert(s[i]);

              bool ok = 1;
            for(auto q : v)
                if(sz(q) > 1)
                    ok = 0;

                 cout<<  ((ok)? "YES\n" : "NO\n");
        }
}