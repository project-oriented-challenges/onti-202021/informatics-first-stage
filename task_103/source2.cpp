//Эффективный перебор
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    int u, p;
    cin >> u >> p;
        while(u--){
            string s;
            cin >> s;
                bool ok = 1;

                for0(i, p){
                    char c = '#';
                for(int j = 0; i+j < sz(s); j += p) {
                    if(s[i+j] != '?')
                        if(c == '#') c = s[i+j];
                    else
                        if(c != s[i+j]) ok = 0;
                }
                }

                 cout<<  ((ok)? "YES\n" : "NO\n");
        }

}