n, p = map(int, input().split())
lines = []
f = open('text.txt', 'w') 
for i in range(n):
    lines.append(input())
for i in range(n):
    line = lines[i]
    q = True
    for j in range(p):
        c = ''
        k = j
        while k < len(line):
            if line[k] != '?':
                if c == '':
                    c = line[k]
                else:
                    if line[k] != c:
                        q = False
                        break
            k += p
        if not q:
            break

   
    if q:
        f.write("YES" + '\n')
    else:
        f.write("NO" + '\n')
f.close()