//Алгоритм Дейкстры

#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define f first
#define s second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;

int dx[4] = {-1, 0, 1, 0},
    dy[4] = {0, 1, 0, -1};
char z[4] = {'n', 'e', 's', 'w'};

signed main(){
    
   ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

   int n, m;
   cin >> n >> m;
   vector<vector<vector<int> > > G(n+1, vector<vector<int> >(m+1));

   string s;
   for0(i, 2*n+1){
    cin >> s;
    if(i%2){
            int tx = i/2;
        for0(j, sz(s))
            if(s[j] == 's')
                G[tx][j].pb(2);
            else
                G[tx+1][j].pb(0);

    }
    else{
        int tx = i/2;
        for0(j, sz(s))
          if(s[j] == 'w')
            G[tx][j+1].pb(3);
          else
            G[tx][j].pb(1);
    }
   }

    string inf;
    inf.resize(6000, '#');
    vector<vector<string> > d(n+1, vector<string>(m+1, inf));
    vector<vector<int> > mark(n+1, vector<int>(m+1, 0));
    d[0][0] = "";

    for0(u, (n+1)*(m+1)){
     int tx, ty, mn = 1e9;
        for0(i, n+1) for0(j, m+1) if(mark[i][j] == 0 && sz(d[i][j]) < mn){
            tx = i;
            ty = j;
            mn = sz(d[i][j]);
        }

        mark[tx][ty] = 1;

        for0(i, sz(G[tx][ty])){
            int tdir = G[tx][ty][i];
            int nx = tx + dx[tdir];
            int ny = ty + dy[tdir];
            string ts = d[tx][ty] + z[tdir];

            if(sz(ts) < sz(d[nx][ny]) || (sz(ts) == sz(d[nx][ny]) && ts < d[nx][ny]))
                d[nx][ny] = ts;
        }
    }

    cout<<sz(d[n][m])<<endl;
    cout<<d[n][m];
}