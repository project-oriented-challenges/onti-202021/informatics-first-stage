//Обход в ширину

#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;

   int n, m;
int dx[4] = {0, -1, 1, 0},
    dy[4] = {1, 0, 0, -1};
char z[4] = {'e', 'n', 's', 'w'};

bool in_sq(int x, int y){

    return (x >= 0 && y >= 0 && x <= 2*n && y <= 2*m);
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    string ans[200][200];
    cin >> n >> m;
    vector<string> G;
    string s;
    for0(i, 2*n+1){
        cin >> s;
        string z;
        if(i%2 == 0){
            for(auto q : s) z = z + "O" + q;
               z += "O";
        }
        if(i%2 == 1){
            for(auto q : s) z = z + q + " ";
        }
        G.pb(z);
    }
    deque<pii> och;
    och.pb({0, 0});
    G[0][0] = '#';
    ans[0][0] = "";

    while(sz(och) > 0){
        pii tp = och[0];
        och.pop_front();

        for0(i, 4){
            int nx = tp.x + dx[i];
            int ny = tp.y + dy[i];
            if(in_sq(nx, ny) && G[nx][ny] == z[i] &&  G[nx + dx[i]][ny + dy[i]] == 'O' ){
                G[nx + dx[i]][ny + dy[i]] = '#';
                ans[nx + dx[i]][ny + dy[i]] = ans[tp.x][tp.y] + z[i];
                och.pb({nx + dx[i], ny + dy[i]});
            }
        }
    }
 
    cout<<sz(ans[2*n][2*m])<<endl;
    cout<<ans[2*n][2*m];
}