def task1():
    vert_num = int(input())
    edge_num = int(input())
    verts = [{"watched": False} for _ in range(vert_num)]
    edges = []
    for _ in range(edge_num):
        edges.append(tuple(int(i)-1 for i in input().split()))

    if vert_num == 0:
        return 'Yes'
    line = [0, 0]
    verts[0]['watched'] = True
    flag = True
    while flag:
        flag = False
        for edge in edges:
            if edge[0] in line or edge[1] in line:
                if edge[0] in line:
                    verts[edge[1]]['watched'] = True
                    if edge[0] == line[0]:
                        line[0] = edge[1]
                    else:
                        line[1] = edge[1]
                else:
                    verts[edge[0]]['watched'] = True
                    if edge[1] == line[0]:
                        line[0] = edge[0]
                    else:
                        line[1] = edge[0]
                edges.remove(edge)
                flag = True
    for v in verts:
        if not v['watched']:
            return 'No'
    if len(edges):
        return 'No'
    return 'Yes'

print(task1())