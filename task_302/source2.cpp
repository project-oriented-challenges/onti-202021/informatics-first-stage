//разделение - знаки подстановки в общее уравнение прямой, площадь – формула
// Герона
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<double, double> pii;
pii A, B;

struct line{
    double A, B, C;
};

line L(pii a, pii b){
    line r;
    r.A = b.y - a.y;
    r.B = a.x - b.x;
    r.C = b.x*a.y - a.x*b.y;

    return r;
}

double dist(pii a, pii b){
    return sqrt((a.x - b.x)*(a.x - b.x)+(a.y - b.y)*(a.y - b.y));
}


double sqt2(pii a, pii b, pii c){
    double p = (dist(a, b) + dist(b, c) + dist(a,c)) / 2.0;
    return 2*(sqrt(p * (p-dist(a,b)) * (p-dist(b,c)) * (p-dist(a,c)) ));
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    cin >> A.x >> A.y >> B.x >> B.y;

    int n, m;
    cin >> n >> m;
    vector<pii> s(n), p(m);
    for0(i, n) cin >> s[i].x >> s[i].y;
    for0(i, m) cin >> p[i].x >> p[i].y;

    double ans = 0;
    line AB = L(A, B);

    for0(i, n)
    ans = max(ans, sqt2(A, B, s[i]) / 2.0);

    for0(j, m)
    ans = max(ans, sqt2(A, B, p[j]) / 2.0);

    for0(i, n){
        pii Bs = {s[i].x - B.x, s[i].y - B.y};
        double stoAB = AB.A*s[i].x + AB.B*s[i].y + AB.C;

        for0(j, m){
            pii Bp = {p[j].x - B.x, p[j].y - B.y};

            double ptoAB = AB.A*p[j].x + AB.B*p[j].y + AB.C;


            if(stoAB < 0 && ptoAB > 0 || stoAB > 0 && ptoAB < 0){
                ans = max(ans, sqt2(A, B, s[i]) * sqt2(A, B, p[j]) / 4.0);
            }
        }
    }

    cout.precision(2);
    cout<<fixed<<ans;
}