//разделение - знаки векторного произведения, площадь - модуль векторного
// произведения
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<int, int> pii;

int vprod(pii a, pii b){
    return a.x*b.y - b.x*a.y;
}

int sgn_vprod(pii a, pii b){
    int vp = vprod(a, b);
    if(vp < 0) return -1;
    if(vp > 0) return 1;
    return 0;
}

int sqt2(pii a, pii b, pii c){
    pii v1 = {b.x - a.x, b.y - a.y};
    pii v2 = {c.x - a.x, c.y - a.y};

    return abs(vprod(v1, v2));
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    pii A, B;
    cin >> A.x >> A.y >> B.x >> B.y;
    pii AB = {B.x - A.x, B.y - A.y};

    int n, m;
    cin >> n >> m;
    vector<pii> s(n), p(m);
    for0(i, n) cin >> s[i].x >> s[i].y;
    for0(i, m) cin >> p[i].x >> p[i].y;

    double ans = 0;

    for0(i, n)
    ans = max(ans, sqt2(A, B, s[i]) / 2.0);

    for0(j, m)
    ans = max(ans, sqt2(A, B, p[j]) / 2.0);

    for0(i, n){
        pii Bs = {s[i].x - B.x, s[i].y - B.y};

        for0(j, m){
            pii Bp = {p[j].x - B.x, p[j].y - B.y};

            if(sgn_vprod(AB, Bs) != sgn_vprod(AB, Bp)){
                ans = max(ans, sqt2(A, B, s[i]) * sqt2(A, B, p[j]) / 4.0);
            }
        }
    }

    cout.precision(2);
    cout<<fixed<<ans;
}