#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<int, int> pii;

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    int n;
    cin >> n;
    vector<int> v(n);
    for0(i, n) cin >> v[i];

    vector<int> suml(n), sumr(n);

    int sum = 0;
    for0(i, n){
        sum += v[i];
        suml[i] = sum;
    }

    sum = 0;
    for(int i = n-1; i >= 0; i--){
        sum += v[i];
        sumr[i] = sum;
    }

     int ans = 0, o1 = 0;
    if(sum % n == 0){
        ans++;
        o1++;
    }

    for(int i = 0; i < n-1; i++){
        if(suml[i] % (i+1) == 0 && sumr[i+1] %(n - i - 1) == 0){
            if(o1 == 1){
                if(suml[i] / (i+1) != sum / n) {
                        ans++;
                }
            }
            else{
                ans++;
            }
        }
    }
    cout << ans;
}