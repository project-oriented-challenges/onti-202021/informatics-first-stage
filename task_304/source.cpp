//два дерева отрезков на сумму

#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;
typedef vector<string> vs;

int n, ans = 0, N = 262144;
vector <int> kl, su;
double eps = 1e-7;

int get(int l, int r, vector<int> &t){
    int res=0;
    for(l+=N, r+=N; l<=r; l=(l+1)>>1, r=(r-1)>>1){
        if(l & 1)
            res += t[l];
        if(!(r & 1))
            res += t[r];
    }
    return res;
}
void upd(int pos, int val, vector<int> &t)
{
    pos += N;
    t[pos] += val;
    for(pos >>= 1; pos; pos >>= 1)
    {
        t[pos] = t[pos * 2] + t[pos * 2 + 1];
    }
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    cout.precision(3);

    cin >> n;
    vector<int> h(n);
    vector<double> ld(n), rd(n), lsa(n), rsa(n);

    for0(i, n)
        cin >> h[i];

    kl.resize(2*N, 0);
    su.resize(2*N, 0);
    double sum = 0;
    for0(i, n){
        sum += h[i];
        lsa[i] = sum / (i+1);
    }

    for0(i, n){
        upd(h[i], 1, kl);
        upd(h[i], h[i], su);

        int gr = lsa[i] + 1 + eps;
        int k = get(gr, N-1, kl);
        double sb = get(gr, N-1, su);

        ld[i] = sb - k * lsa[i];
    }

    kl.resize(0);
    su.resize(0);
    kl.resize(2*N, 0);
    su.resize(2*N, 0);
    sum = 0;
    for(int i = n-1; i >= 0; i--){
        sum += h[i];
        rsa[i] = sum / (n - i);
    }

    for(int i = n-1; i >= 0; i--){
        upd(h[i], 1, kl);
        upd(h[i], h[i], su);

        int gr = rsa[i] + 1 + eps;
        int k = get(gr, N-1, kl);
        double sb = get(gr, N-1, su);

        rd[i] = sb - k * rsa[i];
    }

    double mn = 1e18;
    int a, b;
    for(int i = 1; i < n; i++)
        if(ld[i-1] + rd[i] < mn) {
            mn = ld[i-1] + rd[i];
            a = i;
            b = n - i;
        }
    cout<<a<<' '<<b;
}