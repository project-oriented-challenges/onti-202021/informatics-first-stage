//dfs, нахождение 4-связной области

#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;
typedef vector<string> vs;

int k;
int dx[4] = {-1, 0, 1, 0};
int dy[4] = {0, 1, 0, -1};

bool in_tr(int x, int y, int h, int w){
    return (x >= 0 && y >= 0 && x < h && y < w);
}

void dfs(vector<string> &v, int x, int y){
    v[x][y] = '#';
    k++;
    for0(i, 4){
        int nx = x + dx[i];
        int ny = y + dy[i];

        if(in_tr(nx, ny, sz(v), sz(v[0])) && v[nx][ny] == '$')
            dfs(v, nx, ny);
    }
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    string iss;
    cin >> iss;

    int n = sz(iss);
    for0(i, n) if(iss[i] == '#') iss[i] = '$';
    int kk = 0;
    for(auto q : iss)
        kk += (q == '$');

    for(int i = 5; i < n; i++)
    if(n % i == 0 && n/i >= 5){
        string s = iss;
        vector<string> v;
        for0(j, n/i){
            v.pb(s.substr(0, i));
          if(sz(s) > i)  s = s.substr(i);
        }

        k = 0;
        int sx, sy;
        for0(i, sz(v))
            for0(j, sz(v[i]))
                if(v[i][j] == '$'){
                    sx = i;
                    sy = j;
                }

        dfs(v, sx, sy);

        if(k == kk)
        for0(i, sz(v)) cout<<v[i]<<endl;
    }
}