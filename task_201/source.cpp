//Сравнение с шаблоном, моделирование

#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;
typedef vector<string> vs;

vs knv;
vector<vs > obr(8);
string s;
set<char> ans;

vs r_90(vs &a){
    vs r = a;
    for0(i, sz(a))
        for0(j, sz(a[i])) r[j][sz(a) - i - 1] = a[i][j];
    return r;
}

vs mirr(vs &a){
    vs r = a;
    for0(i, sz(r)) reverse(all(r[i]));
    return r;
}

vs cut(int x, int y, char z){
    vs r;
    for0(i, 5) r.pb(knv[x+i].substr(y, 5));
        for0(i, 5) for0(j, 5) if(r[i][j] != z) r[i][j] = '.'; else r[i][j] = '#';

    return r;
}

int toko(vs &a, char z){
    int r = 0;
    for(auto q : a) for(auto w : q) r += (w == z);
    return r;
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    for0(i, 5){
        cin >> s;
        obr[0].pb(s);
    }

    for1(i, 3) obr[i] = r_90(obr[i-1]);

    obr[4] = mirr(obr[0]);

    for(int i = 5; i < 8; i++) obr[i] = r_90(obr[i-1]);

    int ko = toko(obr[0], '#');

    string z;
    for0(i, 30) z += ".";
    for0(i, 5) knv.pb(z);
    for0(i, 10) {
        cin >> s;
        for0(j, 5) s = "." + s;
        for0(j, 5) s += ".";
        knv.pb(s);
    }
    for0(i, 5) knv.pb(z);

    map<char, int> msk;
    for(auto q : knv)
        for(auto w : q) msk[w]++;

    for(auto q : msk) if(q.y == ko){
            bool ok = 0;
        for0(i, 14) for0(j, 24) {
            vs d = cut(i, j, q.x);

                if(toko(d, '#') == ko){
                    bool ok1;
                    for0(j, 8){
                        if(d == obr[j]){
                                ans.insert(q.x);
                        }
                    }
                }
        }
    }

    for(int i = 5; i < 15; i++){
        for(int j = 5; j < 25; j++)
        if(ans.find(knv[i][j]) != ans.end()) cout<<(char)(knv[i][j] - 'a' + 'A');
        else cout<<knv[i][j];
        cout<<endl;
    }
}