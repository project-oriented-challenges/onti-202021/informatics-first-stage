//префикс-функция, метод Кнута-Морриса-Пратта

#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;
typedef vector<string> vs;

vector<int> pf (string s) {
	int n = sz(s);
	vector<int> pi(n);
	for (int i=1; i<n; ++i) {
		int j = pi[i-1];
		while (j > 0 && s[i] != s[j])
			j = pi[j-1];
		if (s[i] == s[j])  ++j;
		pi[i] = j;
	}
	return pi;
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    int p;
    string s;

    cin >> p >> s;

    vector<int> kmp = pf(s);

    for(int i = sz(kmp)-1; i >= 0; i--)
        if(i - kmp[i] + 1 <= p)
        return cout<<sz(s)-1 - i, 0;
}