//dfs
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<int, int> pii;

int n, z, a, b, q1, q2;
vector<vector<int> > G;
vector<set<int> > rz(2);

void dfs(int a, int p, int dol){
    rz[dol].insert(a);
    for(auto to : G[a]) if(to != p) dfs(to, a, dol);
}

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    cin >> n >> z;
    z--;
    G.resize(n);
    for0(i, n-1){
        cin >> a >> b;
        a--;
        b--;
       if(i != z) {
        G[a].pb(b);
        G[b].pb(a);
       }
       else{
        q1 = a;
        q2 = b;
       }
    }

    dfs(q1, q1, 0);
    dfs(q2, q2, 1);

    vector<int> rzb;
    for(auto q : rz[0]) rzb.pb(q);

    int ans = 0;
    for0(i, sz(rzb)-1){
        if(rzb[i]+1 != rzb[i+1]) ans++;
    }
    if(!(rzb.back() == n-1 && rzb[0] == 0)) ans++;
    cout<<2*ans;
}