//обход в ширину, без глубокой рекурсии
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef long double ld;
typedef pair<int, int> pii;

int n, z, a, b, q1, q2;
vector<vector<int> > G;
vector<int> rz;
int mn = 1e9, aa, bb;

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    cin >> n >> z;
    z--;

    G.resize(n+1);
    for0(i, n-1){
        cin >> a >> b;

        if(i != z){
        G[a].pb(b);
        G[b].pb(a);
        }
        else{
            q1 = a;
            q2 = b;
        }
    }

    vector<int> och;
    vector<int> mark(n+1, -1);
    och.pb(q1);
    mark[q1] = q1;
    int b = 0;

    while(b < sz(och)){
            int v = och[b];
        for(auto to : G[v]) if(mark[to] == -1){
            och.pb(to);
            mark[to] = v;
        }
        b++;
    }

    sort(all(och));

       int ans = 0;
    for0(i, sz(och)-1){
        if(och[i]+1 != och[i+1]) ans++;
    }
    if(!(och.back() == n && och[0] == 1)) ans++;

    cout<<2*ans;
}