//Бинарный поиск
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;


signed main(){
  ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

  int d, b, t, p;

  cin >> d >> b >> t >> p;

  int y = t * (b-d);
  int z = d * p;

  int a = y / z + (y % z != 0);

  int L = 0, R = d * p;

  while(R - L > 1){
    int M = (R + L) / 2;

    if( M * a >= t * (b-d)) R = M; else L = M;

  }

  cout << R;
}