//Стек пар
#include<bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define x first
#define y second

#define all(x) (x).begin(), (x).end()
#define sz(a) (int)(a).size()
#define for0(i, n) for (int i = 0; i < (int)(n); i++)
#define for1(i, n) for (int i = 1; i <= (int)(n); i++)
#define int long long

using namespace std;
typedef pair<int, int> pii;
typedef long double ld;

signed main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    int n;
    cin >> n;
    vector<int> h(n);
    for0(i, n) cin >> h[i];

    vector<pii> st;
    st.pb({2e9, 1});

    for0(i, n){

        if(h[i] < st.back().x) st.pb({h[i], 1});
        else
        if(h[i] == st.back().x) st.back().y++;
        else
        if(h[i] > st.back().x){
            int thb = st.back().x;
            int ost = h[i] - thb;
            int d = 1;
            int v = 0;
            int sump = 0;

            bool ok = 0;
            while(!ok){
                int sp = d * v - sump;

                if(ost <= sp){
                        ok = 1;
                        int tr = ost + sump;
                        int c = tr / d;
                        int o = tr % d;

                        if(o > 0){
                            int thh = thb + c + 1;

                            if(thh ==st.back().x) st.back().y += o;
                            else st.pb({thh, o});
                        }

                        int thh = thb + c;

                        if(thh == st.back().x) st.back().y += (d - o);
                        else st.pb({thh, d - o});
                }
                else{
                    d += st.back().y;
                    v = st[sz(st)-2].x - thb;
                    sump += (st.back().x - thb) * st.back().y;
                    st.pop_back();
                }
            }
        }
    }

    for(int i = 1; i < sz(st); i++)
        for0(j, st[i].y) cout<<st[i].x<<' ';
}