def solve():
    required = 0
    kpps_count = int(input())
    kpps = [0] * kpps_count
    n_notes = int(input())
    visitors = []
    for _ in range(n_notes):
        entry_hour, out_hour,in_kpp,out_kpp = [int(readed) for readed in input().split()]
        visitors.append((entry_hour,in_kpp - 1,True))
        visitors.append((out_hour,out_kpp - 1,False))
    
    visitors.sort(key = lambda visitor: (visitor[0],visitor[2]))
    
    for visitor in visitors:
        if visitor[2]:
            if not kpps[visitor[1]]:
                required += 1
            else:
                kpps[visitor[1]] -= 1
        else:
            kpps[visitor[1]] += 1
    print(required) 

solve()